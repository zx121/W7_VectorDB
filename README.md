# IDS721 Mini Project 7 - Data Processing with Vector Database

## Requirements
* Ingest data into Vector database
* Perform queries and aggregations
* Visualize output

## Main Progress
* `qdrant`: The project utilizes Qdrant, a vector database, for data ingestion, specifically inserting and storing random vectors along with their statistical metadata.
```bash
docker run -p 6334:6334 qdrant/qdrant
# OR sudo docker run -p 6333:6333 -p 6334:6334 qdrant/qdrant
# 6333 port is for dashboard
```
![Qdrant](image.png)

1. __`Data Ingestion into Vector Database`__: The data ingestion into the vector database (Qdrant) should firstly create a Qdrant client and the specific collention. Then it iterates over each vector's statistics, packages them into a payload(empty here), and inserts each vector along with its payload into the Qdrant collection specified by `collection_name`. Here's a quick overview:
```rust
let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    // create a collection
    let collection_name = "vector_collection";
    let _ = client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 10,
                distance: Distance::Cosine.into(), 
                ..Default::default()
            })),
            ..Default::default()
        }),
        ..Default::default()
    }).await;

    // generate and insert random vectors
    let num_vectors = 100; // number of vectors to insert
    for i in 0..num_vectors {
        let vector: Vec<f32> = generate_random_vector(10);
        let empty_payload = Payload::new(); // create an empty payload
        let _ = client.upsert_points_blocking(
            collection_name,
            None,
            vec![PointStruct::new(i, vector, empty_payload)], // here we do not provide payload
            None
        ).await?;
    }

```

2. __`Perform queries and aggregations`__: This part of the code is responsible for performing queries (specifically searching for vectors based on cosine similarity to a query vector). This function sends a search query to the Qdrant database, requesting the closest 5 (which was set as the limit field)  vectors to a specified query vector.
```rust
// perform search
    let query_vector = generate_random_vector(10); // generate a random query vector
    println!("Query Vector: {:?}", &query_vector);

    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.to_string(),
        vector: query_vector,
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;
```

3. __`Visualize Output`__: The visualization of output was used with the `println!` statements. This function prints the search results to the console, displaying each item's information. You can run `cargo lambda watch` to locally observe the results.

```rust
// print search results

    println!("Search Results:");
    for item in search_result.result {
        println!("Data ID: {:?}, Similarity Score: {}", item.id, item.score);
    }
    Ok(())
```

* Visiual Outputs<br/>
![alt text](image-1.png)
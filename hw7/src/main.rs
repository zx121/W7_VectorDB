// 导入必要的库
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{CreateCollection, VectorParams};
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::VectorsConfig;
use qdrant_client::prelude::Distance;
use rand::Rng;
use std::collections::HashMap;
use serde_json::json;
use anyhow::{anyhow, Result};


#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    // create a collection
    let collection_name = "vector_collection";
    let _ = client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 10,
                distance: Distance::Cosine.into(), 
                ..Default::default()
            })),
            ..Default::default()
        }),
        ..Default::default()
    }).await;

    // generate and insert random vectors
    let num_vectors = 100; // number of vectors to insert
    for i in 0..num_vectors {
        let vector: Vec<f32> = generate_random_vector(10);
        let empty_payload = Payload::new(); // create an empty payload
        let _ = client.upsert_points_blocking(
            collection_name,
            None,
            vec![PointStruct::new(i, vector, empty_payload)], // here we do not provide payload
            None
        ).await?;
    }

    // perform search
    let query_vector = generate_random_vector(10); // generate a random query vector
    println!("Query Vector: {:?}", &query_vector);

    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.to_string(),
        vector: query_vector,
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;

    // print search results

    println!("Search Results:");
    for item in search_result.result {
        println!("Data ID: {:?}, Similarity Score: {}", &item, item.score);
    }

    Ok(())
}

// generate a random vector of given dimension
fn generate_random_vector(dim: usize) -> Vec<f32> {
    let mut rng = rand::thread_rng();
    (0..dim).map(|_| rng.gen()).collect()
}
